//
//  TestEffectAU.cpp
//  TestEffectAU
//
//  Created by Chris Nash on 01/10/2014.
//  Copyright (c) 2014 UWE. All rights reserved.
//

#include "TestEffectAU.h"

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

AUDIOCOMPONENT_ENTRY(AUBaseFactory, TestEffectAU)

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Initialize and Cleanup are only called on creation and destruction

OSStatus TestEffectAU::Initialize()
{
    OSStatus res = TestEffectAUBase::Initialize(); // do basic setup
    
    // put your own additional initialisations here (e.g. allocating memory)
    
    fSR= GetSampleRate();
    iBufferSize = (SInt32)(2.0 * fSR);
    pfCircularBuffer = new Float32[iBufferSize];
    
    pfsecondCircularBuffer = new Float32[iBufferSize];
    
    for(int iPos=0 ; iPos < iBufferSize; iPos++)
    {
        pfCircularBuffer[iPos]=0;
    }
    
    iBufferWritePos = 0;
    
    for(int iPos1=0 ; iPos1 < iBufferSize; iPos1++)
    {
        pfsecondCircularBuffer[iPos1]=0;
    }
    
    
    
    iBufferWritePos1 = 0;
    return res;
}

void TestEffectAU::Cleanup()
{
    // put your own additional clean up code here (e.g. free memory)
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void TestEffectAU::PresetLoaded(SInt32 iPresetNum, char *sPresetName)
{
    // a preset has been loaded, so you could perform setup, such as retrieving parameter values
    // using GetParameter and use them to set state variables in the plugin
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void TestEffectAU::OptionChanged(SInt32 iOptionMenu, SInt32 iItem)
{
    // the option menu, with index iOptionMenu, has been changed to the entry, iItem
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void TestEffectAU::ButtonPressed(SInt32 iButton)
{
    // the button, with index iButton, has been pressed
    
    if(iButton == 0)
    {
        pfIndicatorValues[kButton0] = 1;
        pfIndicatorValues[kButton1] = 0;
    }
    else if(iButton == 1)

    {
        pfIndicatorValues[kButton1] = 1;
        pfIndicatorValues[kButton0] = 0;

    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Float32 TestEffectAU::Delay(Float32 x,Float32 fDelTime, SInt32 iBufferReadPos)
{
    
    Float32 fDelSig;
    iBufferWritePos++;
    if( iBufferWritePos > iBufferSize-1)
    {
        iBufferWritePos = 0;
    }
    pfCircularBuffer[iBufferWritePos] = x;
    
    iBufferReadPos = iBufferWritePos - fDelTime * fSR;
    
    if(iBufferReadPos < 0)
    {
        iBufferReadPos+=iBufferSize;
    }
    
    fDelSig = pfCircularBuffer[iBufferReadPos];
    
    return fDelSig;
}

Float32 TestEffectAU::Hardclip(Float32 x, Float32 OverDrive)
{
    x*= OverDrive;
    
    if(x > 1.0)
        x = 1.0;
    else if (x < -1.0)
        x = -1.0;
    
    return x;
}



OSStatus TestEffectAU::ProcessCore(const Float32 *pfInBuffer0,
                                   const Float32 *pfInBuffer1,
                                   Float32 *pfOutBuffer0,
                                   Float32 *pfOutBuffer1,
                                   UInt32 iInStride,
                                   UInt32 iOutStride,
                                   UInt32 inFramesToProcess)
{
    //Float32 fGain = GetParameter(kValue0);
    Float32 fIn0, fIn1;
    Float32 fMix;
    Float32 fOut;
    
    //Delay
    SInt32 iBufferReadPos[2];
    Float32 fDelSig, fDelSig1;
    
    Float32 tapeSpeed;
    
    
    if(pfIndicatorValues[0] == 1)
    {
        tapeSpeed = 7.5;
    }
    else
        tapeSpeed = 15;
    
    Float32 fDelTime = (GetParameter(kValue0) * 3.0 + 1.0 )/tapeSpeed;
    
    
    //Filter
    Float32 fCutOff = 1 / (2 * M_PI * 0.00005);
    
    
    Float32 fFilteredMix;
    
    //signal filtering
    pFilters[kFilter0] -> FilterConfig(kLPF, fCutOff, 0);
    
    pFilters[kFilter1] -> FilterConfig(kBPF, fCutOff, 1000);
    
    //noise filtering
    pFilters[kFilter2] -> FilterConfig(kBRF, 800.0, 40);
    
    //Distortion
    Float32 Overdrive = GetParameter(kValue1) * 15;
    
    Float32 OutGain = GetParameter(kValue2);
    
    //Modulation
    Float32 fFreq = GetParameter(kValue3) * 0.9 + 0.1;
    const Float32 fTwoPI = 2 * M_PI;
    Float32 fPhaseInc =  (fTwoPI * fFreq) / GetSampleRate();
    Float32 fDepth = GetParameter(kValue4) * 0.0015 + 0.0005;
    
    Float32 phase;
    Float32 fOsc;
    
    Float32 fNoise;
    // loop while there are still sample frame to process
    while ( inFramesToProcess-- )
    {
        // read left and right input samples into local variables
        fIn0 = *pfInBuffer0;
        fIn1 = *pfInBuffer1;
        
        // add your processing code here
        
        fMix = (fIn0 + fIn1) * 0.5; // mono mix
        
        
        //Delay
        
        fDelSig=Delay(fMix, fDelTime, iBufferReadPos[0]);
        
        fMix+= fDelSig * 0.2;
        
        //prefilter
        
        fFilteredMix= pFilters[kFilter0] -> Filter(fMix);
        
        // distortion
        
        fFilteredMix = Hardclip(fFilteredMix, Overdrive);
        
        // postfilter
        
        fOut= fFilteredMix + pFilters[kFilter1] -> Filter(fFilteredMix);
        
        //Modulate
        
        fPhasePos += fPhaseInc;
        if(fPhasePos > fTwoPI)
        {
            fPhasePos -= fTwoPI;
        }
        
        phase = sin(fPhasePos) * fDepth;
        
        fOsc = phase + fDepth;
        
        //Delay2
        
        iBufferWritePos1++;
        if( iBufferWritePos1 > iBufferSize-1)
        {
            iBufferWritePos1 = 0;
        }
        pfsecondCircularBuffer[iBufferWritePos1] = fOut;
        
        iBufferReadPos[1] = iBufferWritePos1 - fOsc * fSR;
        
        if(iBufferReadPos[1] < 0)
        {
            iBufferReadPos[1]+=iBufferSize;
        }
        
        fDelSig1 = pfsecondCircularBuffer[iBufferReadPos[1]];
        
        fNoise = (GetParameter(kValue5))*(-0.0005 + (Float32)(rand()) /( (Float32)(RAND_MAX/(0.0015))));
        
        fNoise = pFilters[kFilter2] -> Filter(fNoise);
        
        fOut=fDelSig1 * OutGain + fNoise ;
        
        // write processed local variables to left and right output samples
        *pfOutBuffer0 = fOut ;
        *pfOutBuffer1 = fOut ;
        
        // move pointers along input and output buffers ready for next frame
        pfInBuffer0 += iInStride;
        pfInBuffer1 += iInStride;
        pfOutBuffer0 += iOutStride;
        pfOutBuffer1 += iOutStride;
    }
    
    return noErr;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


